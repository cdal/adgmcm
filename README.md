# README #

This is the code for [exact likelihood inference of Gaussian mixture copulas using Automatic Differentiation](https://arxiv.org/pdf/2010.14359.pdf).

### What is this repository for? ###

* Clustering
* Inference
* and Reproducibility Analysis using Gaussian Mixture Copula Models

### Requirements ###

* Make sure you have a working R and Python installation
* Install GMCM package ([by Bilgrau et al](https://cran.r-project.org/web/packages/GMCM/GMCM.pdf)) in R
* Install RPy2 library in Python to access the GMCM package. RPy2 is used to access the some functionality of 'GMCM' package in R.
* Install autograd library in Python for evaluating the gradients


In case, you are using Anaconda for python, a requirements.txt file is avaialble to create the environment with all the python dependencies.


### Who do I talk to? ###

* Kasa, (kasa@u (dot) nus (dot) edu)
